## Power Curves

###### Aims and Objectives

* To describe the four forces on an aircraft and how they relate to each other with respect to climb performance
* Given specific performance parameters, derive:
  * thrust required
  * thrust available
  * power required
  * power available
* Gain an understanding of the aerodynamic factors that derive Vx and Vy speeds for an aircraft

---

