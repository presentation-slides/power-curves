## Thrust Available

###### Propellers

<p style="text-align: center; font-size: x-large">
  Or in other words, as airspeed of a propeller-driven aircraft increases,
</p>

<p style="text-align: center; font-size: x-large">
  the **Thrust Available** decreases
</p>

---

## Thrust Available

<p style="text-align: center; font-size: x-large">
  We can add this to our drag curve graph
</p>

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-1.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-1.png" alt="Thrust Available Chart 1" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Thrust Available

<p style="text-align: center; font-size: x-large">
  We could then measure the **difference** between
</p>

<p style="text-align: center; font-size: x-large">
  <span style="color: #308ffb">Thrust Available</span> and **Total Drag** at different airspeeds
</p>

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-1.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-1.png" alt="Thrust Available Chart 1" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Thrust Available - Total Drag

##### For example

* <span style="color: red">At 55 knots, with 770 lb of Thrust Available and 400 lb of Total Drag, the difference is **370 lb**</span>
* <span style="color: green">At 90 knots, with 610 lb of Thrust Available and 280 lb of Total Drag, the difference is **330 lb**</span>

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-2.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-2.png" alt="Thrust Available Chart 2" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Thrust Available - Total Drag

<p style="text-align: center; font-size: x-large">
  Find the airspeed at which the difference between <span style="color: #308ffb">Thrust Available</span> and Total Drag
</p>

<p style="text-align: center; font-size: x-large">
  is at its **maximum**
</p>

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-3.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-3.png" alt="Thrust Available Chart 3" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Thrust Available - Total Drag

* For this particular aircraft, at 70 knots, <span style="color: #308ffb">Thrust Available</span> is **700 lb** and Total Drag is **290 lb**
* The leaves a difference between the two of **410 lb**
* There is no other airspeed where this difference is greater than 410 lb &mdash; the **maximum excess thrust**

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-4.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-4.png" alt="Thrust Available Chart 4" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Thrust Available - Total Drag

<p style="text-align: center; font-size: x-large">
  <span style="color: red; font-weight: bold;">This airspeed is the best angle of climb speed, Vx</span>
</p>

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-4.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/thrust-available-chart-4.png" alt="Thrust Available Chart 4" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Best Angle of Climb Speed, Vx

<p style="text-align: center; font-size: x-large">
  In summary, the best angle of climb speed for an aircraft, is determined by the
</p>

<p style="text-align: center; font-size: x-large">
  maximum difference between Thrust Available and Total Drag
</p>

<p style="text-align: center; font-size: x-large">
  the **maximum excess thrust**
</p>

---

