## Revision

###### Drag

* We know that there are two types of drag acting on an aircraft:
  1. Induced Drag
  2. Parasite Drag
* As airspeed increases:
  * Induced Drag decreases
  * Parasite Drag increases
  * *and vice versa*
* The change in both types of drag is **non-linear** with respect to airspeed
* The force of **Drag** opposes the force of **Thrust**

---

## Revision

###### Drag

* As airspeed increases, Induced Drag decreases
* As airspeed increases, Parasite Drag increases

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/drag-curve-1.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/drag-curve-1.png" alt="Drag Curve 1" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Revision

###### Drag

* We can add the Induced Drag to the Parasite Drag
* This will give us the Total Drag

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/drag-curve-2.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/drag-curve-2.png" alt="Drag Curve 2" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Revision

###### Drag

* We can plot the Total Drag on the same graph
* Over airspeed, the Total Drag initially decreases, then increases

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/drag-curve-3.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/drag-curve-3.png" alt="Drag Curve 3" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Revision

###### Climbing

* In straight and level flight, Lift = Weight
* However, to produce a climb, we use thrust
* We apply extra thrust to what would otherwise maintain straight and level flight &mdash; excess thrust
* Specifically, **the vertical component of that excess thrust causes an aircraft to climb**

---

## Revision

###### Propellers

* A propeller generates a force by rotating propeller blade(s)
* The propeller blades generate a lifting force *(thrust)* by pitching at an Angle of Attack

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/propeller-blade-aoa-1.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/propeller-blade-aoa-1.png" alt="Propeller Blade AoA 1" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

## Revision

###### Propellers

* As an aircraft travels forward, the resultant relative air flow on the propeller changes angle
* This reduces the Angle of Attack of the propeller blades, producing less thrust

<p style="text-align: center">
  <a href="https://presentation-slides-media.gitlab.io/power-curves/propeller-blade-aoa-2.png">
    <img src="https://presentation-slides-media.gitlab.io/power-curves/propeller-blade-aoa-2.png" alt="Propeller Blade AoA 2" width="550px" style="border: 4px solid #555"/>
  </a>
</p>

---

