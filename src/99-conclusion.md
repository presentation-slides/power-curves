## Conclusion

1. Total Drag is the sum of Induced and Parasite Drag
2. Climb is caused by excess thrust
3. Thrust Required is equivalent to Total Drag
4. Thrust Available decreases as airspeed increases
5. Best angle of climb speed (Vx) is the airspeed at the maximum amount of excess thrust
6. Power = Thrust x Airspeed
7. Power Required = Thrust Required x Airspeed
8. Power Available = Thrust Available x Airspeed
9. Best rate of climb speed (Vy) is the airspeed at the maximum amount of excess power
